func getAllItems(w http.ResponseWriter, r *http.Request) {
    items, err := dbInstance.GetAllItems()
    if err != nil {
        render.Render(w, r, ServerErrorRenderer(err))
        return
    }
    if err := render.Render(w, r, items); err != nil {
        render.Render(w, r, ErrorRenderer(err))
    }
}
